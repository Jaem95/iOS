//
//  AppDelegate.h
//  Clase_Uno
//
//  Created by José Antonio Escobar Macías on 29/10/16.
//  Copyright © 2016 José Antonio Escobar Macías. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

